interface Props {
  readonly className?: string;
}

const Loader: React.FC<Props> = ({ className }) => {
  return (
    <div className={`text-current w-16 h-16 ${className}`}>
      <svg className='animate-rotate' viewBox='25 25 50 50'>
        <circle
          r='20'
          cx='50'
          cy='50'
          fill='none'
          className='animate-dash stroke-current'
          strokeMiterlimit='10'
          strokeWidth='4'
        />
      </svg>
    </div>
  );
};

export default Loader;
