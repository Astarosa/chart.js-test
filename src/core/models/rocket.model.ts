export interface Rocket {
  "id": number,
  "active": boolean,
  "stages": number,
  "boosters": number,
  "cost_per_launch": number,
  "success_rate_pct": number,
  "first_flight": Date,
  "country": string | null,
  "company": string | null,
  "height": {
    "meters": number,
    "feet": number
  },
  "diameter": {
    "meters": number,
    "feet": number
  },
  "mass": {
    "kg": number,
    "lb": number
  },
  "payload_weights": [
    {
      "id": string | null,
      "name": string | null,
      "kg": number,
      "lb": number
    }
  ],
  "first_stage": {
    "reusable": boolean,
    "engines": number,
    "fuel_amount_tons": number,
    "burn_time_sec": number,
    "thrust_sea_level": {
      "kN": number,
      "lbf": number
    },
    "thrust_vacuum": {
      "kN": number,
      "lbf": number
    }
  },
  "second_stage": {
    "engines": number,
    "fuel_amount_tons": number,
    "burn_time_sec": number,
    "thrust": {
      "kN": number,
      "lbf": number
    },
    "payloads": {
      "option_number": string | null,
      "composite_fairing": {
        "height": {
          "meters": number,
          "feet": number
        },
        "diameter": {
          "meters": number,
          "feet": number
        }
      }
    }
  },
  "engines": {
    "number": number,
    "type": string | null,
    "version": string | null,
    "layout": string | null,
    "engine_loss_max": number,
    "propellant_1": string | null,
    "propellant_2": string | null,
    "thrust_sea_level": {
      "kN": number,
      "lbf": number
    },
    "thrust_vacuum": {
      "kN": number,
      "lbf": number
    },
    "thrust_to_weight": number
  },
  "landing_legs": {
    "number": number,
    "material": string | null
  },
  "wikipedia": string | null,
  "description": string | null,
  "rocket_id": string | null,
  "rocket_name": string | null,
  "rocket_type": string | null
}