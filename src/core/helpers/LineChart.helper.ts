export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
      labels: {
        color: '#fff'
      },
      title: {
        color: '#fff'
      },
    },
    title: {
      display: true,
      text: 'Chart.js Line Chart',
      color: '#fff'
    },
  },
  scales: {
    y: {
      grid: {
        color: '#475569',
        drawBorder: false
      },
      ticks: {
        color: '#fff'
      }
    },
    x: {
      grid: {
        display: false
      },
      ticks: {
        color: '#fff'
      }
    }
  },
};

export const getData = (label: string, labels: string[], data: number[]) => ({
  labels,
  datasets: [
    {
      label,
      data,
      borderColor: '#059669',
      backgroundColor: '#34d399',
      pointRadius: 5,
      pointHoverRadius: 10,
      yAxisID: 'y',
      test: 'blabla'
    },
  ]
});