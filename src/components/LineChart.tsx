import { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import Loader from './Loader';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';

import { Launch } from '@/core/models/launch';
import { getData, options } from '@/core/helpers/LineChart.helper';
import Chart from './Chart';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);


const launchesUrl = "https://api.spacexdata.com/v3/launches";

const LineChart: React.FC = (): JSX.Element => {
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [launches, setLaunches] = useState<Launch[]>([]);

  useEffect(() => {
    const onInit = async () => {
      try {
        const { data } = await axios.get(launchesUrl);
        setLaunches(data);
      } catch (error: any) {
        setError(error.messsage);
        console.log(error);
      } finally {
        setIsLoading(false);
      }
    }
    onInit();
  }, []);


  const getLabels = () => Array.from(new Set(launches.map(({ launch_year }: Launch) => `${launch_year}`))).sort();

  const getLauchesData = () => getLabels().map((label) => launches.reduce((total, current: Launch) => total + (`${current.launch_year}` === label ? 1 : 0), 0));

  if (error) return <div className='w-full h-60 flex items-center justify-center'>{error}</div>;
  if (isLoading) return <div className='w-full h-60 flex items-center justify-center'><Loader /></div>;
  if (!launches.length) return <div className='w-full h-60 flex items-center justify-center'>No data found</div>;

  return <Chart type='line' options={options} data={getData('Rocket Launches', getLabels(), getLauchesData())} />
}

export default LineChart;
