import ChartJsLogo from '@/assets/images/chartjs-logo.svg';
import LineChart from './components/LineChart';
import BarChart from './components/BarChart';

const App: React.FC = (): JSX.Element => {
  return (
    <div className='flex flex-col items-center'>
      <div className='flex items-center my-10'>
        <img className='w-16 mr-4' src={ChartJsLogo} alt="chart.js-logo" />
        <h1 className='text-3xl'>Chart.js</h1>
      </div>
      <div className='flex flex-wrap xl:justify-between justify-center w-full my-10 px-10 md:px-0'>
        <div className='flex flex-col items-center xl:w-1/2 md:w-3/4 w-full md:mb-10 xl:px-10'>
          <LineChart />
        </div>
        <div className='flex flex-col items-center xl:w-1/2 md:w-3/4 w-full xl:px-10'>
          <BarChart />
        </div>
      </div>
    </div>
  );
};

export default App;