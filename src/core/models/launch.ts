export interface Launch {
  "flight_number": number,
  "mission_name": string,
  "mission_id": [],
  "upcoming": false,
  "launch_year": Date,
  "launch_date_unix": Date,
  "launch_date_utc": Date,
  "launch_date_local": Date,
  "is_tentative": boolean,
  "tentative_max_precision": string,
  "tbd": boolean,
  "launch_window": number,
  "rocket": {
    "rocket_id": string,
    "rocket_name": string,
    "rocket_type": string,
    "first_stage": {
      "cores": [
        {
          "core_serial": string,
          "flight": number,
          "block": number,
          "gridfins": boolean,
          "legs": boolean,
          "reused": boolean,
          "land_success": boolean,
          "landing_intent": boolean,
          "landing_type": string,
          "landing_vehicle": string
        }
      ]
    },
    "second_stage": {
      "block": number,
      "payloads": [
        {
          "payload_id": string,
          "norad_id": [],
          "reused": false,
          "customers": [
            "DARPA"
          ],
          "nationality": string,
          "manufacturer": string,
          "payload_type": string,
          "payload_mass_kg": number,
          "payload_mass_lbs": number,
          "orbit": "LEO",
          "orbit_params": {
            "reference_system": string,
            "regime": string,
            "longitude": number,
            "semi_major_axis_km": number,
            "eccentricity": null,
            "periapsis_km": number,
            "apoapsis_km": number,
            "inclination_deg": number,
            "period_min": number,
            "lifespan_years": number,
            "epoch": number,
            "mean_motion": number,
            "raan": number,
            "arg_of_pericenter": number,
            "mean_anomaly": number
          }
        }
      ]
    },
    "fairings": {
      "reused": false,
      "recovery_attempt": false,
      "recovered": false,
      "ship": number
    }
  },
  "ships": [],
  "telemetry": {
    "flight_club": null
  },
  "launch_site": {
    "site_id": string,
    "site_name": string,
    "site_name_long": string
  },
  "launch_success": false,
  "launch_failure_details": {
    "time": number,
    "altitude": number,
    "reason": string
  },
  "details": string,
  "static_fire_date_utc": Date,
  "static_fire_date_unix": Date,
  "timeline": {
    "webcast_liftoff": number
  }
};