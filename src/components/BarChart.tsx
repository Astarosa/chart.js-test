import { useEffect, useState } from "react";
import axios from "axios";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ChartDataset,
} from 'chart.js';

import { Rocket } from '@/core/models/rocket.model';
import Chart from "./Chart";
import Loader from "./Loader";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const rocketsUrl = "https://api.spacexdata.com/v3/rockets";

const BarChart: React.FC = (): JSX.Element => {
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [rockets, setRockets] = useState<Rocket[]>([]);
  const [currentDataset, setCurrentDataset] = useState('engines');

  useEffect(() => {
    const onInit = async () => {
      try {
        const { data } = await axios.get(rocketsUrl);
        setRockets(data);
      } catch (error: any) {
        setError(error.messsage);
        console.log(error);
      } finally {
        setIsLoading(false);
      }
    }
    onInit();
  }, []);

  if (error) return <div className='w-full h-60 flex items-center justify-center'>{error}</div>;
  if (isLoading) return <div className='w-full h-60 flex items-center justify-center'><Loader /></div>;
  if (!rockets.length) return <div className='w-full h-60 flex items-center justify-center'>No data found</div>;

  const options = {
    responsive: true,
    test: 'blabla',
    plugins: {
      legend: {
        position: 'top' as const,
        labels: {
          color: '#fff'
        },
        title: {
          color: '#fff'
        },
      },
      title: {
        display: true,
        text: 'Chart.js Bar Chart',
        color: '#fff'
      },
    },
    scales: {
      y: {
        grid: {
          color: '#475569',
          drawBorder: false
        },
        ticks: {
          color: '#fff'
        }
      },
      x: {
        grid: {
          display: false
        },
        ticks: {
          color: '#fff'
        }
      }
    },
  };

  const labels = Array.from(rockets.map((rocket: Rocket) => rocket.rocket_name));

  const getDataset = (key: string): ChartDataset<'bar', number[]> => {
    switch (key) {
      case 'cost_per_launch': return {
        label: 'Cost per Launch',
        data: labels.map((label) => rockets.find(({ rocket_name }: Rocket) => rocket_name === label)!.cost_per_launch),
        borderColor: '#059669',
        backgroundColor: '#34d399',
        yAxisID: 'y',
      };
      case 'mass': return {
        label: 'Mass by Rocket (Kg)',
        data: labels.map((label) => rockets.find(({ rocket_name }: Rocket) => rocket_name === label)!.mass.kg),
        borderColor: '#059669',
        backgroundColor: '#34d399',
        yAxisID: 'y',
      };
      case 'height': return {
        label: 'Height by Rocket (m)',
        data: labels.map((label) => rockets.find(({ rocket_name }: Rocket) => rocket_name === label)!.height.meters),
        borderColor: '#059669',
        backgroundColor: '#34d399',
        yAxisID: 'y',
      };
      case 'diameter': return {
        label: 'Diameter by Rocket (m)',
        data: labels.map((label) => rockets.find(({ rocket_name }: Rocket) => rocket_name === label)!.diameter.meters),
        borderColor: '#059669',
        backgroundColor: '#34d399',
        yAxisID: 'y',
      };
      default: return {
        label: 'Engines by Rocket',
        data: labels.map((label) => rockets.find(({ rocket_name }: Rocket) => rocket_name === label)!.engines.number),
        borderColor: '#059669',
        backgroundColor: '#34d399',
        yAxisID: 'y',
      };
    }
  }

  const data = {
    labels,
    datasets: [getDataset(currentDataset)]
  }

  return (
    <>
      <Chart type='bar' options={options} data={data} />
      <div className='flex text-sm md:text-base gap-4 mt-10'>
        <button className={`${currentDataset !== 'engines' ? 'opacity-30' : ''}`} onClick={() => setCurrentDataset('engines')}>Engines</button>
        <button className={`${currentDataset !== 'mass' ? 'opacity-30' : ''}`} onClick={() => setCurrentDataset('mass')}>Mass</button>
        <button className={`${currentDataset !== 'height' ? 'opacity-30' : ''}`} onClick={() => setCurrentDataset('height')}>Height</button>
        <button className={`${currentDataset !== 'diameter' ? 'opacity-30' : ''}`} onClick={() => setCurrentDataset('diameter')}>Diameter</button>
        <button className={`${currentDataset !== 'cost_per_launch' ? 'opacity-30' : ''}`} onClick={() => setCurrentDataset('cost_per_launch')}>Cost per Launch</button>
      </div>
    </>
  )

}

export default BarChart;