# React Chart.js demo with SpaceX API
## See the demo [here](https://chart-js-test-7i2z.vercel.app)

### Start the project locally

```bash
yarn dev
```

### Build the project

```bash
yarn build
```
