import { Line, Bar } from "react-chartjs-2";
import { ChartProps } from "react-chartjs-2/dist/types";



interface LineProps extends ChartProps<'line'> {
  type: 'line';
}

interface BarProps extends ChartProps<'bar'> {
  type: 'bar';
}

type Props = LineProps | BarProps;

const Chart: React.FC<Props> = ({ ...props }) => {
  switch (props.type) {
    case 'line': return <Line {...props} />;
    case 'bar': return <Bar {...props} />;
    default: return null;
  }
}

export default Chart;